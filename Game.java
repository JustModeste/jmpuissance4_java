import java.util.Scanner;

/**
 * Classe du jeu 
 */
class Game {
    /**
     * Nombre de colonnes maximale du jeu
     */
    private Integer nbColumns;

    /**
     * Nombre de lignes maximale du jeu
     */
    private Integer nbLines;

	/**
	 * Nombre d'alignement pour gagner
	 */
	private Integer nbAligns;

    /**
     * Nombre de coups maximum possible
     */
    private Integer maxMoves;

    /**
     * Représentation du jeu
     */
    private Character[][] table;

    /**
     * Joueur 1
     */
    private Character player1 = 'X';

    /**
     * Joueur 2
     */
    private Character player2 = 'O';

    /**
     * Représentation des victoires
     */
    private Integer[] victories;

    /**
     * Constructeur
     * @param column 	Nombre de colonnes du jeu
     * @param line  	Nombre de lignes du jeu
	 * @param aligns	Nombre d'alignement total pour gagner
     */
    Game(Integer column, Integer line, Integer aligns) {
        this.nbColumns = column;
		this.nbLines = line;
        this.nbAligns = aligns;
        initVitories();
    }

    /**
     * Initialisation du tableau des victoires
     */
    private void initVitories() {
        this.victories = new Integer[3];

        for (Integer i = 0; i < 3; i++) {
            this.victories[i] = 0;
        }
    }

    /**
     * Initialisation du tableau du jeu
     */
    private void init() {
        this.maxMoves = this.nbColumns * this.nbLines;

        this.table = new Character[this.nbColumns][this.nbLines];
        for (int i = 0; i < this.nbColumns; i++) {
            for (int j = 0; j < this.nbLines; j++) {
                this.table[i][j] = ' ';
            }
        }
    }

    /**
     * Trouve la colonne qui sera joué
     * @param column    Colonne qui veut être jouée
     * @return Integer	La ligne qui peut être utilisée, -1 si la colonne ou la ligne n'est pas possible
     */
    private Integer findLine(Integer column) {
        Integer line = -1;

        if (column > 0 && column <= this.nbColumns) {
            Integer row = 0;

            do {
                if (this.table[column -1][row] == ' ') {
                    line = row;
                }
                row++;
            } while (line == -1 && row < this.nbLines);
        }
        return line;
    }

	/**
	 * Vérifie si le joueur a gagné en fonction de sa position
	 * @param player	Joueur en cours
	 * @param column	Colonne jouée
	 * @param line		Ligne jouée
	 * @return	Boolean	true si le joueur a aligné les NbAligns
	 */
	private Boolean checkVictory(Character player, Integer column, Integer line) {
		Boolean	ret = true;

		ret = checkVerticaly(player, column, line);

		if (!ret) {
			ret = checkHorizontaly(player, column, line);
		}
		if (!ret) {
			ret = checkDiagonally(player, column, line);
		}
		return ret;
	}

	/**
	 * Vérifie l'alignement vertical
	 * @param player	Joueur en cours
	 * @param column	Colonne jouée
	 * @param line		Ligne jouée
	 * @return	Boolean	true si le joueur a aligné les NbAligns
	 */
	private Boolean checkVerticaly(Character player, Integer column, Integer line) {
		Integer aligns = 0;

		for (Integer i = 0 ; i < this.nbAligns && aligns < this.nbAligns; i++) {
			Integer row = line - i;

			if ((row >= 0 && row < this.nbLines) && this.table[column][line - i] == player) {
				aligns++;
			}
		}
		return aligns == this.nbAligns;
	}

	/**
	 * Vérifie les alignements horizontaux
	 * @param player	Joueur en cours
	 * @param column	Colonne jouée
	 * @param line		Ligne jouée
	 * @return	Boolean	true si le joueur a aligné les NbAligns
	 */
	private Boolean checkHorizontaly(Character player, Integer column, Integer line) {
		Integer aligns = 0;
		
		for (Integer i = 0; i < this.nbAligns && aligns < this.nbAligns;  i++) {
			aligns = 0;
			for (Integer j = 0; j < this.nbAligns; j++) {
				Integer col = column - (this.nbAligns - 1) + i + j;

				if ((col >= 0 && col < this.nbColumns) && this.table[col][line] == player) {
					aligns++;
				}
			}
		}
		return aligns == this.nbAligns;
	}

	private Boolean checkDiagonally(Character player, Integer column, Integer line) {
		Integer aligns = 0;

		for (Integer i = 0; i < this.nbAligns && aligns < this.nbAligns; i++) {
			aligns = 0;
			for( Integer j = 0; j < this.nbAligns; j++) {
				Integer col = column - (this.nbAligns -1) + i + j;
				Integer lin = line + (this.nbAligns -1) - i - j;

				if ((col >= 0 && col < this.nbColumns) && (lin >= 0 && lin < this.nbLines)
					&& this.table[col][lin] == player) {
					aligns++;
				}
			}
		}

		if (aligns < this.nbAligns) {
			for (Integer i = 0; i < this.nbAligns && aligns < this.nbAligns; i++) {
				aligns = 0;
				for( Integer j = 0; j < this.nbAligns; j++) {
					Integer col = column - (this.nbAligns -1) + i + j;
					Integer lin = line - (this.nbAligns -1) + i + j;
	
					if ((col >= 0 && col < this.nbColumns) && (lin >= 0 && lin < this.nbLines)
						&& this.table[col][lin] == player) {
						aligns++;
					}
				}
			}
		}

		return aligns == this.nbAligns;
	}

    /**
     * Affichage du tableau de jeu
     */
    public void display() {
        String header = " ";
        for (Integer i = 0; i < this.nbColumns; i++) {
            header += (i + 1) + " ";
        }
        System.out.println(header);
        for (Integer j = this.nbLines - 1; j >= 0; j--) {
            for (Integer i = 0; i < this.nbColumns; i++) {
                System.out.print("|" + this.table[i][j]);
            }
            System.out.println("|");
        }
    }

    /**
     * Lancement du jeu
     */
    public void startGame() {
        this.init();
        this.display();
        Boolean finish = false;
        Integer nbMoves = 1;
        do {
            Character currentPlayer = this.player2;
            Integer numPlayer = 2;
            Integer userColumn = 0;
            Integer nbGame = 0;

            for (Integer i = 0; i < 3; i++) {
                nbGame += this.victories[i];
            }

            if ((nbMoves + nbGame) % 2 == 1) {
                currentPlayer = this.player1;
                numPlayer = 1;
            }
            Integer validLine = 0;
            Scanner in = new Scanner(System.in);
            do {
                System.out.println("Move #" + nbMoves + " : Player " + numPlayer + " (" + currentPlayer + ") - Your move ?");
                userColumn = 0;
                
                try {
                    userColumn = in.nextInt();
                }
                catch (Exception e) {
                    userColumn = -1;
                }
                in.nextLine();

                validLine = findLine(userColumn);

            } while (validLine == -1);
            this.table[userColumn - 1][validLine] = currentPlayer;
            this.display();
			finish = this.checkVictory(currentPlayer, userColumn - 1, validLine);
			nbMoves++;
			if (finish) {
                System.out.println("\n\tPlayer " + numPlayer + " WIN!\n");
                this.victories[numPlayer]++;
			} else {
            	if (nbMoves > this.maxMoves) {
					finish = true;
                    System.out.println("\n\tDRAW!\n");
                    this.victories[0]++;
				}
            }
        } while (!finish);
        displayScores();
    }

    /**
     * Affichage des scores
     */
    public void displayScores() {
        System.out.println("\nScores:");
            for (Integer i = 1; i <= 2;  i++) {
                System.out.println("\tPlayer " + i + ": " + this.victories[i]);
            }
            System.out.println("\tDraw : " + this.victories[0] + "\n");
    }
}