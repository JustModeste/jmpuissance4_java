import java.util.Scanner;

/**
 * Programme principal
 */
public class Puissance4 {
    public static void main(String... args) {
		
		String userChoise = "";
		Scanner in = new Scanner(System.in);
		Game game = new Game(7, 6, 4);

		do {
			// Menu
			System.out.println("\t====");
			System.out.println("\tMENU");
			System.out.println("\t====\n");
			System.out.println("N - New game");
			System.out.println("S - Show Scores");
			System.out.println("Q - Quit");
			do {
				System.out.println("\nYour choice ?");

				userChoise = in.nextLine().trim().toUpperCase();
			} while (!userChoise.equals("N") && !userChoise.equals("Q") && !userChoise.equals("S"));
			if (userChoise.equals("N")) {
				game.startGame();
			}
			if (userChoise.equals("S")) {
				game.displayScores();
			}
		} while (!userChoise.equals("Q"));
		System.out.println("Bye!");
    }
}